<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Контакты</title>
<meta name="keywords" content="free css templates, contact, tripod, blog, theme" />
<meta name="description" content="Tripod Contact Info. - free website template provided by templatemo.com" />
<link href="templatemo_style.css" rel="stylesheet" type="text/css" />

</head>
<body>

<div id="templatemo_header_wrapper">
	<div id="templatemo_header">
    	
       <div id="site_title">
            <h1><a href="index.php">
                <img src="images/templatemo_logo.png" alt="tripod blog" /></a>
            </h1>
        </div>

    </div> <!-- end of header -->
    
    <div id="templatemo_menu">
    
        <ul>
            <li><a href="index.php">Главная</a></li>
            <li><a href="contact.php" class="current">Контакты</a></li>
        </ul>    	
    
    </div> <!-- end of templatemo_menu -->

</div> <!-- end of header wrapper -->

<div id="templatemo_content_wrapper">

	<div id="templatemo_content">
    
    	<h1>Contact Information</h1>

        <p>Validate <a href="" rel="nofollow">XHTML</a> &amp; <a href="" rel="nofollow">CSS</a>. Suspendisse sed odio ut mi auctor blandit. Duis luctus nulla metus, a vulputate mauris. Integer sed nisi sapien, ut gravida mauris. Nam et tellus libero. Cras purus libero, dapibus nec rutrum in, dapibus nec risus. Ut interdum mi sit amet magna feugiat auctor. </p>
       		
            <div class="cleaner_h40"></div>

            <div id="contact_form">
            
                <h2>Quick Contact Form</h2>
                
                <form method="post" name="contact" action="#">
                
                    <input type="hidden" name="post" value="Send" />
                    <label for="author">Name:</label> <input type="text" id="author" name="author" class="required input_field" />
                    <div class="cleaner_h10"></div>
                    
                    <label for="email">Email:</label>
                	<input type="text" id="email" name="email" class="validate-email required input_field" />
                    <div class="cleaner_h10"></div>
                    
                  
                    <label for="text">Message:</label> <textarea id="text" name="text" rows="0" cols="0" class="required"></textarea>
                    <div class="cleaner_h10"></div>
                    
                    <input style="font-weight: bold;" type="submit" class="submit_btn" name="submit" id="submit" value=" Send " />
                    <input style="font-weight: bold;" type="reset" class="submit_btn" name="reset" id="reset" value=" Reset " />
                
                </form>
            
            </div> 
            
    	<div class="cleaner"></div>
    
    </div> <!-- end of templatemo content -->

    <div class="cleaner"></div>
</div> <!-- end of content wrapper -->

<div id="templatemo_footer_wrapper">
	<div id="templatemo_footer">
    
		Copyright © <?=strftime('%Y')?>
        
    </div> <!-- end of templatemo_copyright -->
</div> <!-- end of copyright wrapper -->

</body>
</html>